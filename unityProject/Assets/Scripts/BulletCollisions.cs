﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BulletCollisions : MonoBehaviour
{


     // Start is called before the first frame update
     void Start()
     {

     }

    // Update is called once per frame
    void Update()
    {

    }


     void OnCollisionEnter(Collision coll)
     {
          if (coll.gameObject.tag == "Player")
          {

               Physics.IgnoreCollision(coll.gameObject.GetComponent<Collider>(), this.GetComponent<Collider>());
          }

          if (coll.gameObject.tag == "Wall")
          {
               Destroy(this.gameObject);
          }

          if (coll.gameObject.tag == "AI")
          {
               Destroy(this.gameObject);
          }
     }

     //Goes of camera deletes itself
     void OnBecameInvisible()
     {
          Destroy(this.gameObject);
     }
}
