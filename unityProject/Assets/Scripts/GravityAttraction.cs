﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityAttraction : MonoBehaviour
{
     public Rigidbody rb;
     public PlanetGravity grav;

      void Update()
     {
    
          if (grav.visableTargets != null)
          { 
               if (grav.visableTargets.Count > 0)
               {
                    foreach(Transform vis in grav.visableTargets)
                    {
                         Attract(vis);
                    }
               }
          }
     }

     void Attract(Transform objToAttract)
     {
          Rigidbody rbToAttract = objToAttract.gameObject.GetComponent<Rigidbody>();
          Vector3 direction = rb.position - rbToAttract.position;
          float distance = direction.magnitude;
         
          float forceMag = (rb.mass * rbToAttract.mass) / Mathf.Pow(distance, 2);
          Vector3 force = direction.normalized * forceMag;
          objToAttract.gameObject.GetComponent<Rigidbody>().AddForce(force*8);
     }
}
