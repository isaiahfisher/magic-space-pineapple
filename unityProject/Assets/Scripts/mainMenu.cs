﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class mainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void changeScene(string sceneName){
        SceneManager.LoadScene(sceneName);
    }
    public void exitGame(){
       EditorApplication.isPlaying = false;
       Application.Quit();
    }
}
