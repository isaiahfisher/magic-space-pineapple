﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class chase : MonoBehaviour
{

    public float viewRadius = 25;
    [Range(1, 60)]
    public int castIntervals = 1;
    [Range(0, 360)]
    public float viewAngle = 75;
    [Range(1, 25)]
    public float movementSpeed = 20;
    [Range(30, 60)]
    public float randomOffset = 40;
    [Range(0, 1)]
    public float rotationSpeed = .1f;
    [Range(5, 15)]
    public int distFromPlayer = 8;
    [Range(1, 100)]
    public int flockStrenght = 100;
    [Range(5, 25)]
    public float flockDistance = 20;



    private int count = 0;
    private float currentRotation;
    private float rotationNeeded;
    private Vector3 centerAverage;
    private bool r;


    public LayerMask targetMask;
    public LayerMask obstacleMask;
    public LayerMask alliesMask;

    public GameObject BulletSphere;
    private GameObject bullet;
    private int bulletCount;
    private float nextFire;
    public float fireRate;

    private float oldMovementSpeed;
    private float oldViewRadius;

    private Material mat;


    [HideInInspector]
    public List<Transform> visableTargets = new List<Transform>();
    [HideInInspector]
    public List<Transform> visableAllies = new List<Transform>();

    void Start()
    {
        StartCoroutine("FindTarget", .2f);



        oldMovementSpeed = movementSpeed;
        oldViewRadius = viewRadius;

        nextFire = 1f;
        fireRate = 1f;



        mat = GetComponent<Renderer>().material;

        transform.rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
        currentRotation = transform.rotation.eulerAngles.y;
        currentRotation = rotationNeeded;

    }
    void Update()
    {



        //color change & movement
        mat.color = Color.green;
        AvoidNearbyWalls();
        if (visableTargets.Count == 0)
        {
            transform.position += ((KeepDistance() + transform.forward) * (movementSpeed) * Time.deltaTime);

        }
        //rotation
        //if (currentRotation != rotationNeeded)
        //{
        //    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, rotationNeeded, 0), rotationSpeed);
        //}

        if (visableAllies.Count > 0)
        {
            FindDirection();


        }


        if (visableAllies.Count > 0)
        {
            transform.position += (CenterOfFlock() * movementSpeed * Time.deltaTime);
            // KeepDistance();

        }
        //player targeting
        if (visableTargets.Count > 0)
        {
            mat.color = Color.red;
            // movementSpeed = 5;

            //loops though what has been seen.
            for (int i = 0; i < visableTargets.Count; i++)
            {
                if (visableTargets[i].transform.tag == "Player")
                {
                    transform.LookAt(visableTargets[i].transform);
                    //only moving towards player when outside distance, but always looking at player within visable range.
                    if (Vector3.Distance(transform.position, visableTargets[i].transform.position) > distFromPlayer +movementSpeed)
                    {
                        transform.position += (transform.forward * movementSpeed * Time.deltaTime);
                    }

                    if (!Physics.Raycast(transform.position, visableTargets[i].transform.position, viewRadius, obstacleMask))
                    {
                        if (Time.time > nextFire)
                        {
                            nextFire = Time.time + fireRate;
                            bullet = Instantiate(BulletSphere, transform.position + (gameObject.transform.forward * 2), transform.rotation);
                            bullet.name = "EnemyBullet " + bulletCount.ToString();
                            bullet.tag = "EnemyBullets";
                            bullet.transform.LookAt(visableTargets[i].transform.position);

                            Physics.IgnoreCollision(bullet.GetComponent<Collider>(), visableTargets[i].GetComponent<Collider>());

                            bullet.GetComponent<Rigidbody>().AddForce(transform.forward * 5000f);

                        }



                    }
                    if (visableTargets[i].transform.tag == "Wall")
                    {
                        //code for wall exceptions
                    }
                    if (visableTargets[i].transform.tag == "Asteroid")
                    {
                        //code for asteroid exceptions
                    }
                }
            }
        }
    }



    


    IEnumerator FindTarget(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);


            FindVisibleTargets();

        }

    }


    void LateUpdate()
    {
        if (CollIncoming())
        {
            AvoidNearbyWalls();
            count = 0;
        }
    }

    public void setRotation(Quaternion q)
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, q, rotationSpeed);
    }



    void FindVisibleTargets()
    {



        //clear nearby allies and enemies at the start of a call

        visableTargets.Clear();
        visableAllies.Clear();


        //colliders for nearby enemies and allies
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);


        //temp sets layer different so it that overlap does not detect itself

        int old = gameObject.layer;

        gameObject.layer = 2;
        Collider[] alliesInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, alliesMask);
        gameObject.layer = old;


        //for loop that finds nearby enemys(player)
        float dstToTarget;

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                dstToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
                {
                    visableTargets.Add(target);
                }

            }
        }


        for (int i = 0; i < alliesInViewRadius.Length; i++)
        {
            Transform target = alliesInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;


            //           if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            //           {
            dstToTarget = Vector3.Distance(transform.position, target.position);
            if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
            {
                visableAllies.Add(target);
            }

            //           }
        }


    }


    public Vector3 DirectionFromAngle(float angleDegrees, bool angles)
    {
        //converts angles into vectors to fire drawrays at also used in avoiding walls
        if (!angles)
        {
            angleDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleDegrees * Mathf.Deg2Rad));
    }

    public void AvoidNearbyWalls()
    {
        //angle that raycasts are being fired. along with offset in rotation variable.
        Vector3 directionAngle;
        float offset;

        for (float x = -viewAngle / 2; x < viewAngle / 2; x = x + castIntervals)
        {
            directionAngle = DirectionFromAngle(x, false).normalized * viewRadius;
            if (Physics.Raycast(transform.position, directionAngle, out RaycastHit hit, viewRadius, obstacleMask, QueryTriggerInteraction.UseGlobal))
            {


                offset = Random.Range(20, randomOffset);
                rotationNeeded = transform.rotation.eulerAngles.y + 180 + randomOffset;

                if (visableAllies.Count == 0)
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, rotationNeeded, 0), rotationSpeed);
                }

                if (visableAllies.Count > 0)
                {
                    for (int i = 0; i < visableAllies.Count; i++)
                    {
                        visableAllies[i].transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, rotationNeeded, 0), rotationSpeed);
                    }
                }




            }
        }
    }





    public bool CollIncoming()
    {
        //angle that raycasts are being fired. along with offset in rotation variable.
        Vector3 directionAngle;
        float offset;
        // Debug.Log("Avoid");
        for (float x = -viewAngle / 2; x < viewAngle / 2; x = x + castIntervals)
        {
            directionAngle = DirectionFromAngle(x, false).normalized * viewRadius;
            if (Physics.Raycast(transform.position, directionAngle, out RaycastHit hit, viewRadius, obstacleMask, QueryTriggerInteraction.UseGlobal))
            {

                return true;

            }
        }
        return false;

    }





    //calulates the percieved center of the grouping
    public Vector3 CenterOfFlock()
    {
        Vector3 total = transform.position;
        centerAverage = new Vector3();

        for (int i = 0; i < visableAllies.Count; i++)
        {
            total = total + visableAllies[i].position;
        }
        centerAverage = new Vector3(total.x / (visableAllies.Count + 1), total.y / (visableAllies.Count + 1), total.z / (visableAllies.Count + 1));
        //Debug.Log(centerAverage + " " + " " + (centerAverage/flockStrenght));
        return ((centerAverage - transform.position) / flockStrenght);
    }

    //keeps a small distance away from the other AI
    public Vector3 KeepDistance()
    {
        Vector3 currentPosition = transform.position;
        Vector3 offset = new Vector3(0, 0, 0);
        for (int i = 0; i < visableAllies.Count; i++)
        {
            if (Mathf.Abs(Vector3.Distance(currentPosition, visableAllies[i].transform.position)) < flockDistance)
            {
                offset = offset - (visableAllies[i].transform.position - currentPosition);
                // transform.position = (transform.forward * movementSpeed * Time.deltaTime);
                return offset.normalized;
            }

        }
        return new Vector3(0, 0, 0);

    }



    public void FindDirection()
    {
        Vector3 total = transform.rotation.eulerAngles;
        Vector3 subTotal;
        float distance = 50;



        for (int i = 0; i < visableAllies.Count; i++)
        {

            if (Mathf.Abs(Vector3.Distance(transform.position, visableAllies[i].transform.position)) < distance)
            {
                // visableAllies[i].transform.rotation;
                rotationNeeded = visableAllies[i].transform.rotation.eulerAngles.y;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, rotationNeeded, 0), rotationSpeed);
            }
        }
    }

    // subTotal = new Vector3(0, total.y / (visableAllies.Count + 1), 0);






private void OnDrawGizmosSelected()
{

    Gizmos.color = Color.blue;



        if (visableAllies.Count > 0)
        {
            Gizmos.DrawLine(transform.position, centerAverage);
            Gizmos.DrawSphere(centerAverage, 2f);

        }
    for (float x = -viewAngle / 2; x < viewAngle / 2; x = x + castIntervals)
    {
        Gizmos.DrawRay(transform.position, DirectionFromAngle(x, false).normalized * viewRadius);
    }




}



    }






