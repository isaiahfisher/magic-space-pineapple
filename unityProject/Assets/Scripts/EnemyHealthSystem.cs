﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthSystem : MonoBehaviour
{
     public int maxHealth;
     public int currentHealth;
     public GameObject Effect;


     // Start is called before the first frame update
     void Start()
     {
          currentHealth = maxHealth;

     }

     // Update is called once per frame
     void Update()
     {
          if (currentHealth <= 0)
          {
               gameObject.SetActive(false);
          }
     }

     public void hit(int damage)
     {
          currentHealth -= damage;
     }

     void OnCollisionEnter(Collision coll)
     {
          Instantiate(Effect, this.transform.position, this.transform.rotation);

          if (coll.gameObject.tag == "Bullets")
          {
               this.hit(1);
               this.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
          }
          if (coll.gameObject.tag == "Wall")
          {

               this.hit(2);
               this.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
          }
     }
}
