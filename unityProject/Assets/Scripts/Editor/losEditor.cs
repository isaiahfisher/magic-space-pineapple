﻿using System.Collections;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(chase))]
public class losEditor : Editor
{


    void OnSceneGUI()
    {
        chase fow = (chase)target;
        Handles.color = Color.white;

            Handles.DrawWireArc(fow.transform.position, Vector3.up, Vector3.forward, 360, fow.viewRadius);
        


        Vector3 viewAngleA = fow.DirectionFromAngle(-fow.viewAngle / 2, false);
        Vector3 viewAngleB = fow.DirectionFromAngle(fow.viewAngle / 2, false);

        // Handles.DrawLine(fow.transform.position, fow.transform.position + viewAngleA * fow.viewRadius);
        // Handles.DrawLine(fow.transform.position, fow.transform.position + viewAngleB * fow.viewRadius);


        Handles.color = Color.red;
        foreach (Transform visibleTarget in fow.visableTargets)
        {
            Handles.DrawLine(fow.transform.position, visibleTarget.position);
        }
    }
}
