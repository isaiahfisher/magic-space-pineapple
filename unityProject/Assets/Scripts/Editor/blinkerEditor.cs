﻿using System.Collections;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Blinker))]
public class blinkerEditor : Editor
{
    void OnSceneGUI()
    {
        Blinker fow = (Blinker)target;
        Handles.color = Color.white;

        Handles.DrawWireArc(fow.transform.position, Vector3.up, Vector3.forward, 360, fow.viewRadius);
    }
}

