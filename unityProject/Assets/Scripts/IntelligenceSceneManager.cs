﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntelligenceSceneManager : MonoBehaviour
{ 
    private int gameSelectionVal;
    void Start()
    {
        System.Random rand = new System.Random();
        gameSelectionVal = rand.Next(0,8);
        generateGame(gameSelectionVal);
        
    }
    // Update is called once per frame
    void Update()
    {}

    void generateGame(int gameSelectionVal)
    {
        if (gameSelectionVal == 0) 
        {
            SceneManager.LoadScene("Resources/HyperLuminal/SlidingTilePuzzle/Scenes/sliderPuzzle"); 
        }
        if (gameSelectionVal == 1)
        {
            SceneManager.LoadScene("Resources/HyperLuminal/cipherPuzzle/Scenes/CipherPuzzle");
        }
        if (gameSelectionVal == 2)
        {
            SceneManager.LoadScene("Resources/HyperLuminal/catchingSignals/Scenes/CatchingSignals");
        }
        if (gameSelectionVal == 3)
        {
             SceneManager.LoadScene("Resources/HyperLuminal/SynchronizationMinigame/Scenes/SynchronizationMinigame");
        }
        if (gameSelectionVal == 4)
        {
            SceneManager.LoadScene("Resources/HyperLuminal/assembly/scenes/assemblyMicrogame");
        }
        if (gameSelectionVal == 5)
        {
            SceneManager.LoadScene("Resources/HyperLuminal/balanceBar/scenes/balanceBarMicrogame");
        }
        if (gameSelectionVal == 6)
        {
            SceneManager.LoadScene("Resources/HyperLuminal/quicktime/scenes/quicktimeMicrogame");
        }
        if (gameSelectionVal == 7)
        {
            SceneManager.LoadScene("Resources/HyperLuminal/StrengthBar/scenes/Strength");
        }
    }
}
