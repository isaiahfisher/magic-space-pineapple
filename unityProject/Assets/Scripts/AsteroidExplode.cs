﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidExplode : MonoBehaviour
{
     public GameObject Effect;
     public GameObject Astroid;
     public GameObject AstroidObject;
     public float nextExplosion;
     public float rateExplosion;

     // Start is called before the first frame update
     void Start()
     {

     }

     // Update is called once per frame
     void Update()
     {

     }


     void OnCollisionEnter(Collision coll)
     {
          if (coll.gameObject.tag == "Bullets" || coll.gameObject.tag == "Player")
          {
               Instantiate(Effect, this.transform.position, this.transform.rotation);
               if (transform.localScale.x >=.2f && transform.localScale.y >= .2f && transform.localScale.z >= .2f && Time.time > nextExplosion)
               {
                    transform.localScale -= new Vector3(.2f, .2f, .2f);
               }
               else
               {
                    transform.localScale = new Vector3(0f, 0f, 0f);
               }
               float rand = Random.Range(0f, 1f);
               nextExplosion = Time.time + rateExplosion;
               if (rand == 0)
               {
                    AstroidObject = Instantiate(Astroid, this.transform.position + new Vector3(.5f, .5f, 0.0f), this.transform.rotation);
                    AstroidObject.transform.localScale = this.transform.localScale;
               }
               else
               {
                    AstroidObject = Instantiate(Astroid, this.transform.position + new Vector3(.5f, .5f, 0.0f), this.transform.rotation);
                    AstroidObject.transform.localScale = this.transform.localScale;
                    AstroidObject.transform.localScale -= new Vector3(.2f, .2f, .2f);
               }
          }
     }
}