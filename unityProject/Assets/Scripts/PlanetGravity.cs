﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGravity : MonoBehaviour
{




     public float viewRadius;
     [Range(0, 360)]
     public float viewAngle;
     public float healthTime;
     public LayerMask targetMask;
     public LayerMask obstacleMask;
     private int timer = 0;
     private RaycastHit hitinfo;
     PlayerHealthSystem health;
     Rigidbody rb;
     Material mat;

     [HideInInspector]
     public List<Transform> visableTargets = new List<Transform>();

     void Start()
     {
          StartCoroutine("FindTarget", .2f);
          mat = GetComponent<Renderer>().material;
          rb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>();
          health = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealthSystem>();
          StartCoroutine("DoFireDamage");

     }
     void Update()
     {
          //Debug.Log(visableTargets.Count);
          mat.color = Color.green;



          if (visableTargets.Count > 0)
          {
               mat.color = Color.red;
               // StartCoroutine( "DoFireDamage");
               //for (int i = 0; i < visableTargets.Count; i++)
               //{
               //     transform.LookAt(visableTargets[i].transform);
               //}
               //MoveCloser();
          }
     }


     IEnumerator DoFireDamage()
     {
          while (true)
          {    
               yield return new WaitForSeconds(healthTime);
               //Damage();
          }
     }



     void Damage()
     {
          if (visableTargets.Count > 0)
          {
               health.hit(2);
          }
     }


     //void MoveCloser()
     //{
     //     if (visableTargets.Count > 0)
     //     {
     //          rb.AddForce(GameObject.FindGameObjectWithTag("Planet").transform.forward  * -1 * 10);
     //     }
     //}

     public IEnumerator FindTarget(float delay)
     {
          while (true)
          {
               yield return new WaitForSeconds(delay);
               FindVisibleTargets();
             
          }

     }


     public void FindVisibleTargets()
     {
          visableTargets.Clear();
          Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

          for (int i = 0; i < targetsInViewRadius.Length; i++)
          {
               Transform target = targetsInViewRadius[i].transform;
               Vector3 dirToTarget = (target.position - transform.position).normalized;
               if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
               {
                    float dstToTarget = Vector3.Distance(transform.position, target.position);
                    if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
                    {
                         visableTargets.Add(target);
                    }
                    //Debug.Log(targetsInViewRadius);
               }
          }
     }



     public Vector3 DirectionFromAngle(float angleDegrees, bool angles)
     {
          if (!angles)
          {
               angleDegrees += transform.eulerAngles.y;
          }
          return new Vector3(Mathf.Sin(angleDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleDegrees * Mathf.Deg2Rad));
     }

    

     private void OnDrawGizmosSelected()
     {
          Gizmos.color = Color.red;
          Gizmos.DrawRay(transform.position, transform.forward * 3);
          Gizmos.DrawRay(transform.position, (transform.forward + transform.right).normalized * 3);
          Gizmos.DrawRay(transform.position, (transform.forward - transform.right).normalized * 3);

     }

}
