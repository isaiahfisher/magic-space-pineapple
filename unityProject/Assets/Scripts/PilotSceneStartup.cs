﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilotSceneStartup : MonoBehaviour
{
    [Range(25, 500)]
    public float xCord = 100;
    [Range(25, 500)]
    public float yCord = 100;

    //assigned prefabs for AI, and Asteroids
    public GameObject Asteroid;
    public GameObject Enemy;

    //private List<GameObject> activeEnemies;
    private List<Vector2> enemyLocation;
    private float enemyRand;
    private float rand;
    private GameObject EnemyObject;
    private GameObject AsteroidObject;

    float total;

   

     // Start is called before the first frame update
    void Start()

    {
        //initalizing variables
        // activeEnemies = new List<GameObject>();
        enemyLocation = new List<Vector2>();
        enemyRand = Random.Range(1f, 5f);
        rand = Random.Range(1f, 20f);
        total = enemyRand;


        //loop used to generate AI at random spots on an x and way cordinates
        for (int i=0; i < enemyRand; i++)
        {
            float add = 10;
            float x = Random.Range(-xCord, xCord);
            float y = Random.Range(-yCord, yCord);

            if (enemyLocation != null)
            {
                if (!enemyLocation.Contains(new Vector2(x, y)))
                {

                    if (enemyLocation != null)
                    {
                        if (!enemyLocation.Contains(new Vector2(x, y)))
                        {

                            EnemyObject = Instantiate(Enemy, new Vector3(x, 0, y), this.transform.rotation);
                            EnemyObject.name = i +"";
                            enemyLocation.Add(new Vector2(EnemyObject.transform.position.x, EnemyObject.transform.position.z));
                            
                        }
                        else
                        {
                            while (enemyLocation.Contains(new Vector2(x + add, y + add)))
                            {
                                add += 10;
                            }

                            EnemyObject = Instantiate(Enemy, new Vector3(x + add, 0, y + add), this.transform.rotation);
                            enemyLocation.Add(new Vector2(EnemyObject.transform.position.x, EnemyObject.transform.position.z));
                        }
                    }
                }
            }
        }


                 
            
        

        //loop used to generate asteroids within same cord field as AI
        for (int a = 0; a< rand; a++)
        {
            float add = 10;
            float x = Random.Range(-xCord, xCord);
            float y = Random.Range(-yCord, yCord);
            if (!enemyLocation.Contains(new Vector2(x, y)))
            {
                AsteroidObject = Instantiate(Asteroid, new Vector3(x, 0, y), this.transform.rotation);
                AsteroidObject = Instantiate(Asteroid, new Vector3(x, 0, y), this.transform.rotation);
            }
            else
            {
                while (enemyLocation.Contains(new Vector2(x + add, y + add)))
                {
                    add += 10;
                }

                AsteroidObject = Instantiate(Asteroid, new Vector3(x + add, 0, y + add), this.transform.rotation);
            }
        }
    }

   void Update()
    {
        Debug.Log(GameObject.FindGameObjectsWithTag("AI").Length);
        if (GameObject.FindGameObjectsWithTag("AI").Length < (3 +total/5))
        {
            
            total += 5;
            spawnMoreEnenies(total);
        }


    }

    void spawnMoreEnenies(float amount)
    {
        //initalizing variables
        // activeEnemies = new List<GameObject>();
        Debug.Log("test");
        enemyLocation = new List<Vector2>();
        //enemyRand = Random.Range(1f, amount);
        rand = Random.Range(1f, 20f);




        //loop used to generate AI at random spots on an x and way cordinates
        for (int i = Mathf.RoundToInt(amount); i < amount+5; i++)
        {
            
            float add = 10;
            float x = Random.Range(-xCord, xCord);
            float y = Random.Range(-yCord, yCord);

            if (enemyLocation != null)
            {
                if (!enemyLocation.Contains(new Vector2(x, y)))
                {

                    if (enemyLocation != null)
                    {
                        if (!enemyLocation.Contains(new Vector2(x, y)))
                        {

                            EnemyObject = Instantiate(Enemy, new Vector3(x, 0, y), this.transform.rotation);
                            EnemyObject.name = "Clone " + i;
                            enemyLocation.Add(new Vector2(EnemyObject.transform.position.x, EnemyObject.transform.position.z));
                        }
                        else
                        {
                            while (enemyLocation.Contains(new Vector2(x + add, y + add)))
                            {
                                add += 10;
                            }

                            EnemyObject = Instantiate(Enemy, new Vector3(x + add, 0, y + add), this.transform.rotation);
                            enemyLocation.Add(new Vector2(EnemyObject.transform.position.x, EnemyObject.transform.position.z));
                        }
                    }
                }
            }
        }

    }
}
