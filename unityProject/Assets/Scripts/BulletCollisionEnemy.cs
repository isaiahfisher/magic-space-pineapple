﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollisionEnemy : MonoBehaviour
{

     // Start is called before the first frame update
     void Start()
     {

     }

     // Update is called once per frame
     void Update()
     {

     }


     void OnCollisionEnter(Collision coll)
     {
          if (coll.gameObject.tag == "Player")
          {
               Destroy(this.gameObject);
          }

          if (coll.gameObject.tag == "Wall")
          {
               Destroy(this.gameObject);
          }

          if (coll.gameObject.tag == "AI")
          {
               Physics.IgnoreCollision(coll.gameObject.GetComponent<Collider>(), this.GetComponent<Collider>());
          }
     }

     //Goes of camera deletes itself
     void OnBecameInvisible()
     {
          Destroy(this.gameObject);
     }
}
