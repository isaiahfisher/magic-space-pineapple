﻿using System.Collections;
using UnityEngine;
using System;

public class Movement : MonoBehaviour
{
     public float topVelocity;
     public float AccelerationSpeed;
     public float horizontalSpeed;
     private float oldVelocityRightx;
     private float oldVelocityLeftx;
     private float velocityRightx;
     private float velocityLeftx;
     public float verticalSpeed;
     private float oldVelocityUpy;
     private float oldVelocityDowny;
     private float velocityUpy;
     private float velocityDowny;
     public AudioSource engineMoving;
     public AudioSource engineIdle;
     public AudioSource engineSlow;
     // Start is called before the first frame update
     void Start()
     {
          engineIdle.Play();
         
        if (topVelocity == 0)
          {
               topVelocity = 10.0f;
          }
          if (AccelerationSpeed == 0)
          {
               AccelerationSpeed = 0.2f;
          }
     }

    // Update is called once per frame
    void Update()
    {
          //Move right
          if (Input.GetAxisRaw("Horizontal") > 0.5f)
          {
               InputRightPushed();
          }

          //Move left
          if (Input.GetAxisRaw("Horizontal") < -0.5f)
          {
               InputLeftPushed();
          }

          //Move up
          if (Input.GetAxisRaw("Vertical") > 0.5f)
          {
               InputUpPushed();
          }

          //Move down
          if (Input.GetAxisRaw("Vertical") < -0.5f)
          {
               InputDownPushed();
          }

          //Braking
          if (Input.GetAxisRaw("Jump") > .05f)
          {
               Stoping();
               EngineSlowingSound();
          }

          //Point the ship in mouse direction
          LookAtMouseLocation();
          //Move the ship
          transform.transform.Translate(new Vector3(horizontalSpeed * Time.deltaTime , 0f ,verticalSpeed * Time.deltaTime ),Space.World);
          EngineSound();
     }

     void EngineSound()
     {
          if((horizontalSpeed!=0 || verticalSpeed != 0) && !engineMoving.isPlaying)
          {
               if (engineSlow.isPlaying)
               {
                    engineSlow.Stop();
               }
               engineMoving.Play();
          }
     }

     void EngineSlowingSound()
     {
          if (!engineSlow.isPlaying)
          {
               if (engineMoving.isPlaying && (horizontalSpeed == 0 || verticalSpeed == 0))
               {
                    engineMoving.Stop();
               }
               engineSlow.Play();
          }

     }

     float velocityLeft()
     {
          float accel = acceleration();
          if (oldVelocityLeftx > -topVelocity) { 
               velocityLeftx = (float)oldVelocityLeftx - (float)accel;
               oldVelocityLeftx = velocityLeftx;
          }
          return velocityLeftx;
     }

     float velocityRight()
     {
          float accel = acceleration();
          if (oldVelocityRightx < topVelocity)
          {
               velocityRightx = (float)oldVelocityRightx + (float)accel;
               oldVelocityRightx = velocityRightx;
          }
          return velocityRightx;
     }

     float velocityDown()
     {
          float accel = acceleration();
          if (oldVelocityDowny > -topVelocity)
          {
               velocityDowny = (float)oldVelocityDowny - (float)accel;
               oldVelocityDowny = velocityDowny;
          }
          return velocityDowny;
     }

     float velocityUp()
     {
          float accel = acceleration();
          if (oldVelocityUpy < topVelocity)
          {
               velocityUpy = (float)oldVelocityUpy + (float)accel;
               oldVelocityUpy = velocityUpy;
          }
          return velocityUpy;
     }

     float acceleration()
     {
          return 1.0f * (float)Math.Sin(AccelerationSpeed);
     }

     void LookAtMouseLocation()
     {
          var mouse = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,10));
          var screenPoint =new Vector3(transform.position.x, transform.position.y, transform.position.z);
          var offset = new Vector2( screenPoint.x- mouse.x , screenPoint.z- mouse.z);
          var angle = (Mathf.Atan2(offset.y, offset.x) * Mathf.Rad2Deg);
          transform.rotation = Quaternion.Euler(0, -1*(angle+90), 0);
     }

     void Stoping()
     {
          if (velocityUpy > 0)
          {
               velocityUpy -= acceleration();
               oldVelocityUpy = velocityUpy;
          }

          if (velocityUpy < .5)
          {
               velocityUpy = 0;
               oldVelocityUpy = velocityUpy;
          }

          if (velocityDowny < 0)
          {
               velocityDowny += acceleration();
               oldVelocityDowny = velocityDowny;
          }

          if (velocityDowny > -.5)
          {
               velocityDowny = 0;
               oldVelocityDowny = velocityDowny;
          }

          if (velocityRightx > 0)
          {
               velocityRightx -= acceleration();
               oldVelocityRightx = velocityRightx;
          }

          if (velocityRightx < .5)
          {
               velocityRightx = 0;
               oldVelocityRightx = velocityRightx;
          }

          if (velocityLeftx < 0)
          {
               velocityLeftx += acceleration();
               oldVelocityLeftx = velocityLeftx;
          }

          if (velocityLeftx > -.5)
          {
               velocityLeftx = 0;
               oldVelocityLeftx = velocityLeftx;
          }
          verticalSpeed = (velocityUpy + velocityDowny);
          horizontalSpeed = (velocityRightx + velocityLeftx);
     }

     void InputDownPushed()
     {
          velocityDown();

          if (velocityUpy > 0)
          {
               velocityUpy += velocityDowny;
               oldVelocityUpy = velocityUpy;
          }

          verticalSpeed = (velocityUpy + velocityDowny);
     }

     void InputUpPushed()
     {
          velocityUp();

          if (velocityDowny < 0)
          {
               velocityDowny += velocityUpy;
               oldVelocityDowny = velocityDowny;
          }

          verticalSpeed = (velocityUpy + velocityDowny);
     }

     void InputLeftPushed()
     {
          velocityLeft();

          if (velocityRightx > 0)
          {
               velocityRightx += velocityLeftx;
               oldVelocityRightx = velocityRightx;
          }

          horizontalSpeed = (velocityRightx + velocityLeftx);
     }

     void InputRightPushed()
     {
          velocityRight();

          if (velocityLeftx < 0)
          {
               velocityLeftx += velocityRightx;
               oldVelocityLeftx = velocityLeftx;
          }

          horizontalSpeed = (velocityRightx + velocityLeftx);
     }
}
