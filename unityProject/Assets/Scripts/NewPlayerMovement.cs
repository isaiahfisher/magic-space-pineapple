﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPlayerMovement : MonoBehaviour
{
     public float topVelocity;
     public float oldVelocityUp;
     public float oldVelocityDown;
     public float velocityUp;
     public float velocityDown;
     public float driftVelocity;
     public float accelerationSpeed;
     public AudioSource engineMoving;
     public AudioSource engineIdle;
     public AudioSource engineSlow;
     public Rigidbody rigbody;
     // Start is called before the first frame update
     void Start()
     {
          engineIdle.Play();

          if (topVelocity == 0)
          {
               topVelocity = 10.0f;
          }
          if (accelerationSpeed == 0)
          {
               accelerationSpeed = 0.2f;
          }
     }

     // Update is called once per frame
     void Update()
     {
          //Move right
          if (Input.GetAxisRaw("Horizontal") > 0.5f)
          {
               RotateRight();
          }

          //Move left
          if (Input.GetAxisRaw("Horizontal") < -0.5f)
          {
               RotateLeft();
          }

          //Move up
          if (Input.GetAxisRaw("Vertical") > 0.5f)
          {
               VelocityUp();
               engineIdle.Stop();
               EngineSound();
          }

          //Move down
          if (Input.GetAxisRaw("Vertical") < -0.5f)
          {
               VelocityDown();
               MoveDown();
               EngineSlowingSound();
          } 

          if(Input.GetAxisRaw("Jump") > 0.5f)
          {
               Stopping();
          }
     }

     private void FixedUpdate()
     {
          rigbody.velocity = ((transform.forward * velocityUp) + (transform.right  * driftVelocity));

          if (driftVelocity > .5)
          {
               driftVelocity -= Acceleration();
          }
          else if (driftVelocity < -.5)
          {
               driftVelocity += Acceleration();
          }else
          {
               driftVelocity = 0;
          }
     }

     void EngineSound()
     {
          if (!engineMoving.isPlaying)
          {
               if (engineSlow.isPlaying)
               {
                    engineSlow.Stop();
               }
               engineMoving.Play();
          }
     }

     void EngineSlowingSound()
     {
          if (!engineSlow.isPlaying)
          {
               if (engineMoving.isPlaying)
               {
                    engineMoving.Stop();
               }
               engineSlow.Play();
          }
     }

     void EngineStop()
     {
          if (velocityUp == 0)
          {
               if (!engineIdle.isPlaying)
               {
                    if (engineMoving.isPlaying)
                    {
                         engineMoving.Stop();
                    }
                    else if (engineSlow.isPlaying)
                    {
                         engineSlow.Stop();
                    }
                    engineIdle.Play();
               }
          }
     }

     void Stopping()
     {
          if (velocityUp > 0)
          {
               velocityUp -= Acceleration();
               oldVelocityUp = velocityUp;
          }

          if (velocityUp < .5)
          {
               velocityUp = 0;
               oldVelocityUp = velocityUp;
          }

          if (velocityDown < 0)
          {
               velocityDown += Acceleration();
               oldVelocityDown = velocityDown;
          }

          if (velocityDown > -.5)
          {
               velocityDown = 0;
               oldVelocityDown = velocityDown;
          }
     }

     void MoveDown()
     {
          velocityUp += velocityDown;
          oldVelocityUp = velocityUp;
          oldVelocityDown = 0;
          velocityDown = 0;
     }

     void VelocityDown()
     {
          float accel = Acceleration();
          if (oldVelocityDown > -topVelocity)
          {
               velocityDown = (float)oldVelocityDown - (float)accel;
               oldVelocityDown = velocityDown;
          }
     }

     void VelocityUp()
     {
          float accel = Acceleration();
          if (oldVelocityUp < topVelocity)
          {
               velocityUp = (float)oldVelocityUp + (float)accel;
               oldVelocityUp = velocityUp;
          }
     }

     void RotateRight()
     {
          transform.Rotate(0.0f, 2.0f, 0.0f, Space.World);
          if (driftVelocity > -10)
          {
               driftVelocity -= 0.5f;
          }
     }

     void RotateLeft()
     {
          transform.Rotate(0.0f, -2.0f, 0.0f, Space.World);
          if (driftVelocity < 10)
          {
               driftVelocity += 0.5f;
          }
     }

     float Acceleration()
     {
          return 1 * (float)Math.Sin(accelerationSpeed);
     }
}
