﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthSystem : MonoBehaviour
{
     public int maxHealth;
     public int currentHealth;
     public GameObject Effect;
     public GameObject DamageBorder;
     private float damageGone;
     public float damageRate;

     // Start is called before the first frame update
     void Start()
    {
          damageRate = 0.25f;
          DamageBorder.SetActive(false);
          currentHealth = maxHealth;
          
     }

    // Update is called once per frame
    void Update()
    {
          if (currentHealth <= 0)
          {
               gameObject.SetActive(false);
          }
          if(damageGone <= Time.time)
          {
               DamageBorder.SetActive(false);
          }
    }

     public void hit(int damage)
     {
         // this.GetComponent<Rigidbody>().(new Vector3((-1)*this.GetComponent<Movement>().horizontalSpeed*2.11f, (-1) * this.GetComponent<Movement>().verticalSpeed * 2.11f, 0f));
          currentHealth -= damage;
          GameObject.FindGameObjectWithTag("Health").transform.localScale = new Vector3(GameObject.FindGameObjectWithTag("Health").transform.localScale.x - .2f, GameObject.FindGameObjectWithTag("Health").transform.localScale.y, GameObject.FindGameObjectWithTag("Health").transform.localScale.z);
          this.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
          DamageBorder.SetActive(true);
          damageGone = Time.time + (damage/maxHealth);
     }

     void OnCollisionEnter(Collision coll)
     {
          if (coll.gameObject.tag == "Bullets")
          {
               Physics.IgnoreCollision(coll.gameObject.GetComponent<Collider>(), this.gameObject.GetComponent<Collider>());
               this.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
          }
          if (coll.gameObject.tag == "Wall")
          {
               Instantiate(Effect, this.transform.position, this.transform.rotation);
               this.hit(2);
          }
          if (coll.gameObject.tag == "AI")
          {
               Instantiate(Effect, this.transform.position, this.transform.rotation);
               this.hit(2);
          }
          if (coll.gameObject.tag == "Planet")
          {
               Instantiate(Effect, this.transform.position, this.transform.rotation);
               this.hit(2);
               this.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
          }
          if (coll.gameObject.tag == "EnemyBullets")
          {
               Instantiate(Effect, this.transform.position, this.transform.rotation);
               this.hit(2);
          }
     }
}
