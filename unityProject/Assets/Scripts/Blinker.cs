﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinker : MonoBehaviour
{

    public float viewRadius = 50;

    [Range(1, 60)]
    public int castIntervals = 1;


    public LayerMask targetMask;
    public LayerMask obstacleMask;

    Vector3 directionAngle;
    Vector3 location;

    private float nextActionTime = 0.0f;
    public float period = 5f;

    List<float> open;
    List<Transform> visableTargets;

    public GameObject BulletSphere;
    private GameObject bullet;
    private int bulletCount;
    private float nextFire;
    public float fireRate;

    private Color alphaColor;
    private float timeToFade = 1f;

    private void Start()
    {
        StartCoroutine("FindTarget", .2f);
        open = new List<float>();
        visableTargets = new List<Transform>();

        nextFire = 1f;
        fireRate = 1f;

        alphaColor = GetComponent<MeshRenderer>().material.color;
        alphaColor.a = 0;
    }

    private void Update()
    {
        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            hopFinder();
        }
        if (visableTargets != null)
        {
            for (int i = 0; i < visableTargets.Count; i++)
            {
                if (visableTargets[i].transform.tag == "Player")
                {
                    transform.LookAt(visableTargets[i].transform);
                    //only moving towards player when outside distance, but always looking at player within visable range.
                    if (!Physics.Raycast(transform.position, visableTargets[i].transform.position, viewRadius, obstacleMask))
                    {
                        if (Time.time > nextFire)
                        {
                            nextFire = Time.time + fireRate;
                            bullet = Instantiate(BulletSphere, transform.position + (gameObject.transform.forward * 2), transform.rotation);
                            bullet.name = "EnemyBullet " + bulletCount.ToString();
                            bullet.tag = "EnemyBullets";
                            bullet.transform.LookAt(visableTargets[i].transform.position);
                            Physics.IgnoreCollision(bullet.GetComponent<Collider>(), visableTargets[i].GetComponent<Collider>());
                            bullet.GetComponent<Rigidbody>().AddForce(transform.forward * 5000f);
                        }
                    }
                    if (visableTargets[i].transform.tag == "Wall")
                    {
                        //code for wall exceptions
                    }
                    if (visableTargets[i].transform.tag == "Asteroid")
                    {
                        //code for asteroid exceptions
                    }
                }
            }
        }
        
        
    }

    IEnumerator FindTarget(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            targetFinder();
        }

    }

    public Vector3 DirectionFromAngle(float angleDegrees, bool angles)
    {
        //converts angles into vectors to fire drawrays at also used in avoiding walls
        if (!angles)
        {
            angleDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleDegrees * Mathf.Deg2Rad));
    }

    private void hopFinder()
    {
        open.Clear();
        for (float x = 0; x < 360; x += castIntervals)
        {
            directionAngle = DirectionFromAngle(x, false).normalized * viewRadius;
            if (Physics.Raycast(transform.position, directionAngle, out RaycastHit hit, viewRadius, obstacleMask, QueryTriggerInteraction.UseGlobal))
            {

            }
            else
            {
                open.Add(x);
            }
        }
        
        float randomAngle = open[Random.Range(0, open.Count)];
        float randomDistance = Random.Range(0, viewRadius);

        location = transform.position + DirectionFromAngle(randomAngle, false).normalized * randomDistance;

        transform.position = location;

    }

    private void targetFinder()
    {
        //clear nearby allies and enemies at the start of a call

        visableTargets.Clear();

        //colliders for nearby enemies and allies
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

        //for loop that finds nearby enemys(player)
        float dstToTarget;

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
                dstToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
                {
                    visableTargets.Add(target);
                }       
        }
    }



    private void OnDrawGizmosSelected()
    {
        for (float x = 0; x < 360; x += castIntervals)
        {
            directionAngle = DirectionFromAngle(x, false).normalized * viewRadius;
            if (Physics.Raycast(transform.position, directionAngle, out RaycastHit hit, viewRadius, obstacleMask, QueryTriggerInteraction.UseGlobal))
            {
                Gizmos.color = Color.red;
                Gizmos.DrawRay(transform.position, DirectionFromAngle(x, false).normalized * viewRadius);
                Gizmos.DrawSphere(transform.position + DirectionFromAngle(x, false).normalized * viewRadius, 1f);
            }
            else
            {
                Gizmos.color = Color.green;
                Gizmos.DrawRay(transform.position, DirectionFromAngle(x, false).normalized * viewRadius);
                Gizmos.DrawSphere(transform.position + DirectionFromAngle(x, false).normalized * viewRadius, 1f);
            }


        }
        if (visableTargets != null)
        {
            for (int x = 0; x < visableTargets.Count; x++)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(transform.position, visableTargets[x].position);
            }


            Gizmos.color = Color.magenta;
            Gizmos.DrawSphere(location, 1f);
        }  

    }
}