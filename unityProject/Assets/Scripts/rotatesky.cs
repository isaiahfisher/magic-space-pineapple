﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotatesky : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //rotates the sky by changing the shader float value in the skybox shader
        //to adjust the current position vector every frame
        RenderSettings.skybox.SetFloat("_Rotation", Time.time * 2);
    }
    //both OnApplicationQuit and OnDisable have the body which sets the shader position values back to their default
    //this prevents the scene from loading in absurd orientations
    void OnApplicationQuit()
    {
        //sets the value of the float back to zero so the skybox always starts in the proper location
        RenderSettings.skybox.SetFloat("_Rotation", 0);
    }
    void OnDisable()
    {
        RenderSettings.skybox.SetFloat("_Rotation",0);
    }
}
