﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LaunchMissile : MonoBehaviour
{
    private GameObject bullet;
    private GameObject muzzle;
    public GameObject BulletSphere;
    public GameObject muzzleFab;
    private float nextFire;
    public float fireRate;
    private int bulletCount;
    public AudioSource lazerSound;
    public int missileCount;

    private Vector3 mousePos;
    // Start is called before the first frame update
    void Start()
    {
        bulletCount = 0;
        nextFire = 1f;
        fireRate = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire && missileCount != 0)
        {
            lazerSound.Play();
            nextFire = Time.time + fireRate;
          //  var dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
           // var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            
            //Debug.Log(ray.direction.y);
           var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            mousePos = new Vector3(ray.direction.x, 0, ray.direction.z).normalized + transform.position;

            Debug.Log(Input.mousePosition);
           // Debug.Log(mousePos);
            CreateBullet();
            //  bullet.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);


            //mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z - transform.position.z));
            bullet.transform.LookAt(mousePos);


            /// bullet.transform.LookAt(transform.position + Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0)).direction.normalized);
            //   bullet.GetComponent<Rigidbody>().AddForce((Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)).direction.normalized) * 10000f);
            bulletCount++;
            GameObject.FindGameObjectWithTag("Ammo").transform.localScale = new Vector3(GameObject.FindGameObjectWithTag("Ammo").transform.localScale.x - .1f, GameObject.FindGameObjectWithTag("Ammo").transform.localScale.y, GameObject.FindGameObjectWithTag("Ammo").transform.localScale.z);
            missileCount--;
        }
    }


    void CreateBullet()
    {
        muzzle = Instantiate(muzzleFab, transform.position + (transform.forward*2), transform.rotation);
        bullet = Instantiate(BulletSphere, transform.position,transform.rotation);
        bullet.name = "Bullet " + bulletCount.ToString();
        bullet.tag = "Bullets";
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        //   Debug.Log(Camera.main.ViewportPointToRay(Input.mousePosition));
        Gizmos.DrawSphere(mousePos, 2f);
    }
}
