﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CipherWheel : MonoBehaviour
{ 
    //the prefab for the tile objects
    public GameObject tilePrefab;
    //the prefab for the alienRunes
    public GameObject alienRunePrefab;
    //the alphabet tracking functionality
    private int alienLanguageCursor;
    //the cursor of the outer wheel
    private int outerWheelCursor;
    //words are a wordbank for the cypher puzzle
    private string[] words = {"ASSIMILATE", "DOMINION", "DESTROY", "HARVEST", "VICTORY"};
    //answer is the word the player is trying to guess
    private string answer;
    //numberOfGuesses tracks number of guesses remaining
    private int numberOfGuesses;
    //win is checked on update, if true, it will end the puzzle with reward
    public bool win;
    //fail is checked on update, if true, it will end the puzzle with punishment
    private bool fail;
    //an array of tiles to hold the coded word
    private GameObject[] codedWordTiles;
    //an array of tiles to hold the player guessed word tiles
    private GameObject[] playerWordTiles;
    //an array of the alien runes sprites that are rendered in scene
    private GameObject[] codedSprites;
    //an array of the regular letter sprites that rendered in scene
    private GameObject[] playerSprites;
    //the array of possible alienRunes
    public Object[] alienRunes;
    //an array of letter sprites
    public Object[] letterSprites;
    //tracks which letter of the word is currently selected
    private int currLetterTracker = 0;
    //the innerWheel object
    private GameObject wheelHolder;
    // Start is called before the first frame update
    void Start()
    {
        //instantiates on scene start
        //The array of alien runes is loaded
        alienRunes = Resources.LoadAll("HyperLuminal/cipherPuzzle/Art/alienLanguageTextures", typeof(Sprite));
        //The array of letter assets is loaded
        letterSprites = Resources.LoadAll("HyperLuminal/cipherPuzzle/Art/decipheredLanguage", typeof(Sprite));
        //instantiate the innerWheel
        wheelHolder = GameObject.Find("InnerWheel");
        //fail and win both set to false on Instantiation
        fail = false;
        win = false;
        //randomizes the alien language solution
        System.Random rand = new System.Random();
        alienLanguageCursor = rand.Next(0,26);
        //calls the alienLanguageBuilder method with alienLanguageCursor
        alienLanguageBuilder(alienLanguageCursor);
        //called the wordGenerator method to select a word to encode.
        answer = wordGenerator(); 
        //instantiates the array of player sprite positions 
        playerSprites = new GameObject[answer.Length];
        //call the wordGridBuilder to build the tile grid
        wordGridBuilder(answer.Length);
        //number of guesses initially set to twice the length of the word.
        numberOfGuesses = answer.Length*2;
    }

    // Update is called once per frame
    void Update()
    {
        //checks win/fail every frame
        if(win == true){
           winHandler(); 
        }
        if(fail == true){
           loseHandler(); 
        }
    }

    //generates the answer from the list of possible answers
    public string wordGenerator(){
        //random int used as index to a predefined dictionary of words.
        System.Random rand = new System.Random();
        string word = words[rand.Next(0,4)];
        //returns the word to be encoded.
        return word;
    }

    //this method initializes the tile arrays to the proper size and location based on the word that has been selected by the word generator method
    public void wordGridBuilder(int wordLength){
        //first initialize both GameObject arrays to the proper size
        codedWordTiles = new GameObject[wordLength];
        playerWordTiles = new GameObject[wordLength];
        codedSprites = new GameObject[wordLength];
        float x = (float) -2.0;
        float xSprite = (float) -969.55;
        //next begin building each individual tile out and storing it in the arrays.
        for (int i = 0; i < wordLength; i++)
        {
            codedWordTiles[i] = Instantiate(tilePrefab,new Vector3((float) (x+(i*.13)),(float) .82,(float) -8.55),Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
            codedWordTiles[i].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, true); 
            codedSprites[i] = Instantiate(alienRunePrefab, new Vector3((float) (xSprite+(i*.8745)),(float) -538,(float) 0), Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
            codedSprites[i].GetComponent<SpriteRenderer>().sprite =(Sprite) alienRunes[(int)answer[i]-65];
            codedSprites[i].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
            playerWordTiles[i] = Instantiate(tilePrefab,new Vector3((float) (x+(i*.13)),(float) .534,(float) -8.55),Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
            playerWordTiles[i].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, true);
        } 
    }

    //properly orders the array of alien runes in order to randomize the language as per the cursor
    //works by circular shift of the alienRune array.
    //The method used for rotation is the standard reversal algorithm for circular array rotation
    //the process involves reversing two separate parts of the array
    //then reversing a final time the entirety of the array to give the proper
    //left shift of the array
    //This algorithm ensures up to 26 unique languages while using the same wheel asset
    public void alienLanguageBuilder(int cursor)
    {
        //no need to do reversals if cursor is 0
        //therefore return for optimization purposes
        if (cursor == 0)
            return;

        //reverses the array from element 0 to element cursor -1
        System.Array.Reverse(alienRunes, 0, cursor);
        //reverses the array from element cursor to element 25
        System.Array.Reverse(alienRunes, cursor, 26-cursor);
        //reverses the entire array resulting in a proper left shift of #cursor# spaces
        //causing the new language to follow the wheel convention
        System.Array.Reverse(alienRunes, 0, 26);
    }
    
    //implements the logic for guessing a letter and determining if said letter is correct.
    //will subtract from number of guesses on each attempt.
    public void guessALetter()
    {
        numberOfGuesses--;
        if (currLetterTracker == answer.Length - 1)
        {
            //todo check if letter is correct
            if (wheelHolder.GetComponent<moveWheel>().getCursTracker() == (int)answer[currLetterTracker]-65)
            {
            //if correct
                playerSprites[currLetterTracker] = Instantiate(alienRunePrefab, new Vector3((float) (-969.55+(currLetterTracker*.8745)), (float) -539.85,(float) 0), Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
                playerSprites[currLetterTracker].GetComponent<SpriteRenderer>().sprite = (Sprite) letterSprites[(int)answer[currLetterTracker]-65];
                playerSprites[currLetterTracker].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
                win = true;
            }
            else
            {
                Debug.Log("incorrect guess");
            }
        } else
        {
            if (wheelHolder.GetComponent<moveWheel>().getCursTracker() == (int)answer[currLetterTracker]-65)
            {
                playerSprites[currLetterTracker] = Instantiate(alienRunePrefab, new Vector3((float) (-969.55+(currLetterTracker*.8745)), (float) -539.85,(float) 0), Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
                playerSprites[currLetterTracker].GetComponent<SpriteRenderer>().sprite = (Sprite) letterSprites[(int)answer[currLetterTracker]-65];
                playerSprites[currLetterTracker].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
                currLetterTracker++;
                Debug.Log("correct Letter");
            } else
            {
                Debug.Log("incorrect Letter");
            }
        }
        if (numberOfGuesses == 0 && win == false)
            fail = true;
    }
    
    //handles the minigame win condition
    public void winHandler()
    {
            //victory condition
            SceneManager.LoadScene("Scenes/Intelligence");
    }
    
    //handles the minigame lose condition
    public void loseHandler()
    {
        //fail condition
        SceneManager.LoadScene("Scenes/Intelligence");
    }

}
