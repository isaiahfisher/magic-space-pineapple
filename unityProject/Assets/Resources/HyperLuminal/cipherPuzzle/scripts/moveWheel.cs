﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveWheel : MonoBehaviour
{
    private int symbolTracker;
    private int cursorTracker;
    // Start is called before the first frame update
    void Start()
    {
        symbolTracker = 0;
        cursorTracker = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator rotationAnimation()
    {
        if (symbolTracker < 25)
        {
            symbolTracker++;
        }
        else
        {
            symbolTracker = 0;
        }
        for (int i = 0; i < 20; i++)
        {
            yield return new WaitForSeconds(.05f);
            GameObject.Find("InnerWheel").transform.Rotate(0,0,0.6925f);
        }
    }
    IEnumerator rotationAnimationCursor()
    {
        if (cursorTracker < 25)
        {
            cursorTracker++;
        }
        else
        {
            cursorTracker = 0;
        }
        for (int i = 0; i < 20; i++)
        {
            yield return new WaitForSeconds(.05f);
            GameObject.Find("Cursor").transform.Rotate(0,0,-0.6925f);
        }
    }
    IEnumerator rotationAnimationCursorReverse()
    {
        if (cursorTracker > 0)
        {
            cursorTracker--;
        }
        else
        {
            cursorTracker = 25;
        }
        for (int i = 0; i < 20; i++)
        {
            yield return new WaitForSeconds(.05f);
            GameObject.Find("Cursor").transform.Rotate(0,0,0.6925f);
        }
    }

    public void rotateWheel()
    {
        StartCoroutine(rotationAnimation());
    }

    public int getSymTracker()
    {
        return symbolTracker;
    }

    public void moveCursor()
    {
        StartCoroutine(rotationAnimationCursor());
    }

    public void moveCursorReverse()
    {
        StartCoroutine(rotationAnimationCursorReverse());
    }

    public int getCursTracker()
    {
        return cursorTracker;
    }
}
