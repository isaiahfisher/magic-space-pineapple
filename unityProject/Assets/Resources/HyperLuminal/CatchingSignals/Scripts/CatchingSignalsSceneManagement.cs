﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;
using UnityEngine.SceneManagement;

public class CatchingSignalsSceneManagement : MonoBehaviour
{
    private GameObject[] signals;
    private Vector3 mouse;
    private int signalScore;
    private int goodGen = 0;
    private int badGen = 0;
    public GameObject goodSignal;
    public GameObject badSignal;
    private Vector3 pos;
    // Start is called before the first frame update
    void Start()
    {
        pos = new Vector3(-495,450,0);
        signalScore = 0;
        signals = new GameObject[10];
        generateSignal();

    }

    // Update is called once per frame
    void Update()
    {
        
        if(transform.position.x <= 450)
        {
            transform.position = new Vector3(450, transform.position.y,transform.position.z);
        }
        else if(transform.position.x >= 1550)
        {
            transform.position = new Vector3(1550, transform.position.y,transform.position.z);
        }
        else if(!(Input.mousePosition.x >= 1550) && !(Input.mousePosition.x <= 450))
        {
            transform.position = new Vector3(Input.mousePosition.x, transform.position.y ,transform.position.z);
        }

    }
    //this calls the internal coroutine
    public void generateSignal()
    {
        StartCoroutine(generateSignalInternal());
    }
    //this method will randomize the order and location signals are sent in
    private IEnumerator generateSignalInternal()
    {
        int x = -495;
        //initialize a randomizer
        System.Random randomizer = new System.Random();
        //randomize the order of the signals in the array
        for (int i = 0; i < 10; i++)
        {
            if (goodGen < 3 && badGen < 7)
            {
                int choice = randomizer.Next(0,2);
                if(choice == 0)
                {
                    signals[i] = Instantiate(goodSignal,pos,Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
                    signals[i].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
                    goodGen++;
                } else
                {
                    signals[i] = Instantiate(badSignal,pos,Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
                    signals[i].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
                    badGen++;
                }
        
            } else if (goodGen < 3)
            {
                signals[i] = Instantiate(goodSignal,pos,Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
                signals[i].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
                goodGen++;
            } else if (badGen < 7)
            {
                signals[i] = Instantiate(badSignal,pos,Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
                signals[i].transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
                badGen++;
            }
            x += 300;
            if (x > 550)
                x = -350;
            pos = new Vector3(x,450,0);
            yield return new WaitForSeconds(1);
        }
    }

    //this method handles winning the game
    public void win()
    {
        //TODO: win the game
        SceneManager.LoadScene("Scenes/Intelligence");
    }

    //this method handles losing the game
    public void lose()
    {
        //TODO: lose the game
        SceneManager.LoadScene("Scenes/Intelligence");
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == "BadSignal")
        {
            Destroy(c.gameObject);
            this.lose();
           
        } else if (c.gameObject.tag == "GoodSignal")
        {
            Destroy(c.gameObject);
            signalScore++;
            if(signalScore >= 3){
                this.win();
            }
        }
    }
}
