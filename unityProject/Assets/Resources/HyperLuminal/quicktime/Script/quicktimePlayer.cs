﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class quicktimePlayer : MonoBehaviour
{

     public Vector2 jump;
     public float jumpForce;
     public bool isGrounded;
     Rigidbody2D rb;
     public float velocity;
          // Start is called before the first frame update
          void Start()
          {
               rb = GetComponent<Rigidbody2D>();
               jump = new Vector2(0.0f, 1.0f);
               isGrounded = true;
               rb.velocity = new Vector2(velocity, 0f); 
          }

    // Update is called once per frame
    void Update()
    {
          if (isGrounded == true)
          {
               rb.velocity = new Vector2(velocity, 0f);
          }
          if (this.gameObject.transform.position.x > 110)
       {
           //TODO: win
           SceneManager.LoadScene("Scenes/Intelligence");
       }
       if (Input.GetButtonDown("Jump") && isGrounded == true)
       {
               rb.AddForce(jump * jumpForce, ForceMode2D.Impulse);
               isGrounded = false;
       }
    }

    public void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Wall")
          {
              //TODO: lose
              SceneManager.LoadScene("Scenes/Intelligence");
          }if(col.gameObject.tag == "Bridge")
          {
               isGrounded = true;
          }
    }
}
