﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class balanceBar : MonoBehaviour
{

     public bool inverse;
     public float resizeAmount;
     public string resizeDirection;
     GameObject indicator;
     // Start is called before the first frame update
     void Start()
     {
          indicator = GameObject.FindGameObjectWithTag("Finish");
          indicator.transform.position = new Vector3(Random.Range(-30, 30), 0f, 0f);
     }

     // Update is called once per frame
     void FixedUpdate()
     {
          if (Input.GetButtonDown("Jump") )
          {
               if((transform.position.x + (transform.localScale.x * 4) + (resizeAmount * 4)) > (indicator.transform.position.x - (indicator.transform.localScale.x * 4)) && (transform.position.x + (transform.localScale.x * 4) + (resizeAmount * 4)) < (indicator.transform.position.x + (indicator.transform.localScale.x * 4)))
               {
                    win();
               }
               else
               {
                    lose();
               }
          }
          if ((transform.position.x + (transform.localScale.x * 4) + (resizeAmount * 4)) < 60f && inverse == false)
          {
               resize(resizeAmount, resizeDirection);
          }else if (transform.localScale.x > 0f)
          {
               inverse = true;
               resize(resizeAmount, resizeDirection);
          }
          else
          {
               inverse = false;
          }
     }

     void resize(float amount, string direction)
     {
          if (direction == "x" && inverse == false)
          {
               transform.position = new Vector3(transform.position.x + (amount *4 ), transform.position.y, transform.position.z);
               transform.localScale = new Vector3(transform.localScale.x + amount, transform.localScale.y, transform.localScale.z);
          }
    

          if (direction == "x" && inverse == true)
          {
               transform.position = new Vector3(transform.position.x - (amount *4), transform.position.y, transform.position.z);
               transform.localScale = new Vector3(transform.localScale.x - amount, transform.localScale.y, transform.localScale.z);
          }
         

     }
     //handles the win condition for the minigame
     public void win()
     {
          //TODO: win
          SceneManager.LoadScene("Scenes/Intelligence");
     }

     //handles the lose condition for the minigame
     public void lose()
     {
          //TODO: lose
          SceneManager.LoadScene("Scenes/Intelligence");
     }
}
