﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncStartup : MonoBehaviour
{
     int count;
    // Start is called before the first frame update
    void Start()
    {
          //GameObject.FindGameObjectWithTag("Bridge").transform.position = new Vector3(0.0f,0.0f,5.0f);
          //GameObject.FindGameObjectWithTag("Bridge").gameObject.GetComponent<Renderer>().enabled = false;
          foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Wall"))
          {
               obj.gameObject.GetComponent<Renderer>().enabled = false;
          }
          count = 0;

     }

    // Update is called once per frame
    void Update()
    {
          wallButton wallsbutt = GameObject.FindGameObjectWithTag("BadSignal").GetComponent<wallButton>();
          bridgeButton bbutt = GameObject.FindGameObjectWithTag("GoodSignal").GetComponent<bridgeButton>();
          if (bbutt.hit == true && wallsbutt.hit == true)
          {
               if (count == 0)
               {
                    GameObject.FindGameObjectWithTag("Energy").gameObject.SetActive(false);
                    count++;
               }
               //GameObject.FindGameObjectWithTag("Bridge").gameObject.GetComponent<Renderer>().enabled = true;
               //GameObject.FindGameObjectWithTag("Bridge").transform.position = new Vector3(0.0f, -30.0f, -5.0f);
          }
    }
}
