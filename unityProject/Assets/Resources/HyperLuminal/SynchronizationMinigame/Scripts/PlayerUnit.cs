﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerUnit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
         
    }

    // Update is called once per frame
    void Update()
    {
          if (Input.GetKey("w"))
          {
                   gameObject.transform.position += new Vector3(0.0f,1.0f,0.0f);
          }
          if (Input.GetKey("a"))
          {
               gameObject.transform.position += new Vector3(-1.0f, 0.0f, 0.0f);
          }
          if (Input.GetKey("s"))
          {
               gameObject.transform.position += new Vector3(0.0f, -1.0f, 0.0f);
          }
          if (Input.GetKey("d"))
          {
               gameObject.transform.position += new Vector3(1.0f, 0.0f, 0.0f);
          }
          
     }

     private void OnCollisionEnter2D(Collision2D collision)
     {
          if (collision.gameObject.tag == "Player")
          {
               SceneManager.LoadScene("Scenes/Intelligence");
          }
          if (collision.gameObject.tag == "Wall")
          {
               SceneManager.LoadScene("Scenes/Intelligence");
          }
     }


     //this method handles winning the game
     public void win()
     {
          //TODO: win the game
          SceneManager.LoadScene("Scenes/Intelligence");
     }
}
