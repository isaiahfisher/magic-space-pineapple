﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SynchronizationMinigameSceneManagement : MonoBehaviour
{
    //this is the array of player drone units for the minigame
    private GameObject[] playerUnits;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //this method handles winning
    void win()
    {
        SceneManager.LoadScene("Scenes/Intelligence");
    }

    //this method handles losing
    void lose()
    {
        SceneManager.LoadScene("Scenes/Intelligence");
    }
}
