﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SliderPuzzleSceneManagement : MonoBehaviour
{
    //puzzlePreFab refers to the slider puzzle prefab. It is used to generate
    //all Slider Puzzles.
    //possibleImages is an array of texture resources that can be used to randomly
    //generate the proper image for a puzzle prefab.
    public Object[] possibleImages;
    // Start is called before the first frame update
    public GameObject puzzlePreFab;
    private GameObject currentPuzzle;
    private float xRot;
    private Vector3 pos;
    // Start is called before the first frame update
    void Start()
    {
        //using System.Random to generate random sizes for height and width
        System.Random rand = new System.Random();
        int choice = rand.Next(3,6);
        int choice2 = rand.Next(3,6);
        //sets the values of the height and width to the prefab
        puzzlePreFab.GetComponent<ST_PuzzleDisplay>().Height = choice;
        puzzlePreFab.GetComponent<ST_PuzzleDisplay>().Width = choice2;
        //loads all image resources for possible textures and sets the image as the texture of the 
        //prefab
        possibleImages = Resources.LoadAll("HyperLuminal/SlidingTilePuzzle/Art/SliderPuzzleImages", typeof(Texture2D));
        Texture2D puzzleTexture = (Texture2D)possibleImages[rand.Next(0, possibleImages.Length)];
        puzzlePreFab.GetComponent<ST_PuzzleDisplay>().PuzzleImage = puzzleTexture;
        //instantiates the prefab in the correct area of the screen.
        xRot = (float) -89.98;
        pos = new Vector3(0,0,0);
        currentPuzzle = Instantiate(puzzlePreFab, pos, Quaternion.Euler(new Vector3(xRot,0,0)));
    }

    // Update is called once per frame
    void Update()
    {
        //win condition: puzzle is completed
        if (currentPuzzle.GetComponent<ST_PuzzleDisplay>().Complete)
        {
           SceneManager.LoadScene("Scenes/Intelligence"); 
        } //TODO: define lose condition
    }
}
