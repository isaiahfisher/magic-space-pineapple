﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GeneratingBar : MonoBehaviour
{
     public GameObject redPrefab;
     public GameObject yellowPrefab;
     public GameObject greenPrefab;
     private GameObject[] green;
     float length;
     int tileCounter;
     float nextTile;
     bool top;

    // Start is called before the first frame update
    void Start()
    {
          length = Random.Range(7, 9);
          tileCounter = 0;
          nextTile = 0.2f;
          top = false;
          green = new GameObject[(int)length+1];
         // GenerateTiles();
     }

     // Update is called once per frame
     void Update()
     {
          if (Input.GetButtonDown("Jump")&& green[tileCounter-1].gameObject.name=="redPrefab")
          {
              win();
          }
          if (Time.time > nextTile)
          {
               //Debug.Log(tileCounter);
               GenerateTiles();
               tileCounter = CheckTileAmount();       
               nextTile +=  0.2f;
          }
     }

     public void GenerateTiles()
     {
          if (top == false)
          {
              
               if (tileCounter < 3)
               {
                    green[tileCounter] = Instantiate(greenPrefab, new Vector3(0.0f, (float)tileCounter*10, 0.0f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

               }
               else if (tileCounter < 5)
               {
                    green[tileCounter] = Instantiate(yellowPrefab, new Vector3(0.0f, (float)tileCounter*10, 0.0f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
               }
               else
               {
                    green[tileCounter] = Instantiate(redPrefab, new Vector3(0.0f, (float)tileCounter*10, 0.0f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                    green[tileCounter].gameObject.name = "redPrefab";
               }
          }
          else
          {
              green[tileCounter].gameObject.GetComponent<DestroyTiles>().DestroyTile();
          }
     }

     public int CheckTileAmount()
     {
          if(top == true)
          {
          
               if(tileCounter ==0)
               {
                    top = false;
                    return tileCounter;
               }
               tileCounter--;
          }
          else
          {
              
               if (tileCounter ==length)
               {
                    top = true;
                    //tileCounter--;
                    return tileCounter;
               }
               tileCounter++;
          }
          return tileCounter;
     }

     public void VisibleTiles()
     {

     }

     //handles the microgame win condition
     public void win()
     {
         //TODO: win
        SceneManager.LoadScene("Scenes/Intelligence");
     }
}
