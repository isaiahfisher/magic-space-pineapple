﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomCollision : MonoBehaviour
{
     MiddleCollision midd = new MiddleCollision();
     private void OnCollisionEnter2D(Collision2D collision)
     {
          if(collision.gameObject.tag == "Bridge")
          {
               midd.lose();
          }
          if(collision.gameObject.tag == "BadSignal")
          {
               midd.win();
          }
     }
}
