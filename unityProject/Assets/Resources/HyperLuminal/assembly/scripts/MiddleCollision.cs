﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MiddleCollision : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
     private void OnCollisionEnter2D(Collision2D collision)
     {
          if(collision.gameObject.tag == "Bridge")
          {
               lose();
          }
          if(collision.gameObject.tag == "GoodSignal" && collision.gameObject.tag == "Finish")
          {
               win();
          }
     }

     //handles the win condition for the minigame
     public void win()
     {
          //TODO: win
          SceneManager.LoadScene("Scenes/Intelligence");
     }

     //handles the lose condition for the minigame
     public void lose()
     {
          //TODO: lose
          SceneManager.LoadScene("Scenes/Intelligence");
     }
}
