﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class assemblyScript : MonoBehaviour
{
     public GameObject[] pieces;
     GameObject droppedPiece;
     int counter = 0;
     private float nextFire;
     public float fireRate;
     public int numberOfPieces;
     bool right = true;
     // Start is called before the first frame update
     void Start()
    {
          nextFire = 1f;
          fireRate = 1f;
     }

     // Update is called once per frame
     void Update()
     {
          if(Input.GetButtonDown("Jump") && Time.time > nextFire)
          {
               nextFire = Time.time + fireRate;
               if (counter < numberOfPieces)
               {
                    Drop();
               }
          }
          if (gameObject.transform.position.x < 100 && right == true)
          {
               gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(50f, 0f);
          }
          else if (gameObject.transform.position.x > -100)
          {
               gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-50f, 0f);
               right = false;
          }
          else
          {
               right = true;
          }
     }


     void Drop()
     {
          droppedPiece = Instantiate(pieces[counter], transform.position , transform.rotation);
          counter++;
     }
    
   
}
