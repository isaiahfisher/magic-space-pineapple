﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class energy : MonoBehaviour
{
    public int energyAmount = 10;       //amount of energy to deliver to generator
    public int moveMultiplier = 1;      //movement multiplier along conveyor belts
    public Rigidbody rb;                //energy's rigibody
    public Collider col;                //energy's collider
    public GameObject energySpawner;
    public GameObject switchHolder;     //find switcholder to determine when to change direction
    public GameObject[] slots;          //slots where energy can be stored
    public GameObject trash;            //trash destroyer
    public GameObject rootObj;          //root object
    public GameObject duplicate;        //duplicate object
    public int speed = 20;              //speed to move between slots
    public float secondsWait = 3;       //wait time in seconds at a slot
    public int slotPosition = 0;        //position in slots

    protected int counter = 0;          // counter for timing movement

    private Vector3 pointer;            //mouse pointer position
    private Vector3 slotPos;            //vector3 position of a slot
    private Vector3 trashPos;           //vector3 position of trash
    private bool initialMove = true;    //set initial movement along slots
    private bool haveMoved = false;     //check if energy has moved
    private bool collidedWithEnergy = false;    //check for collisions with other energy
    private bool moveUp = false;        //assign upward movement
    private bool moveLeft = false;      //assign leftward movement
    private bool moveRight = false;     //assign rightward movement
    private bool hasDuplicate = false;  //has had a duplicate created
    private bool checkForDup = false;   //does the duplicate still exist?
    private bool successPlaced = false; //placed on the correct thing

    /*Start()
     * assign energy's collider
     * find switchHolder
     * create array of slots and assign slots to the positions of the array
     * find trash
     */
    void Start()
    {
        col = GetComponent<BoxCollider>();
        energySpawner = GameObject.Find("Energy Spawner");
        switchHolder = GameObject.Find("SwitchHolder");
        slots = new GameObject[3];
        slots[0] = GameObject.Find("Slot 1");
        slots[1] = GameObject.Find("Slot 2");
        slots[2] = GameObject.Find("Slot 3");
        trash = GameObject.Find("Trash");

        secondsWait = energySpawner.GetComponent<spawner>().getSpawnDelay();
    }

    /*FixedUpdate()
     * Check what movement state the energy is assigned to.
     * Assign the energy to the appropriate movement state.
     * Check for duplicate energy in order to allow the user to drag a copy and have both behave correctly
     */
    void FixedUpdate()
    {
        if (initialMove)
        {
            InitialMovement();
        }

        if(moveUp)
        {
            UpwardMovement();
        }

        if(moveLeft)
        {
            LeftMove();
        }

        if(moveRight)
        {
            RightMove();
        }

        if (checkForDup && hasDuplicate)
        {
            if (duplicate == null)
            {
                hasDuplicate = false;
                checkForDup = false;
                Destroy(gameObject);
            }
        }
    }

    /* setCollidedWithEnergy(bool choice)
     * collidedWithEnergy setter
     */
    private void setCollidedWithEnergy(bool choice)
    {
        collidedWithEnergy = choice;
    }

    /* OnTriggerEnter(Collider other)
     * using tag, check what the energy has entered the trigger of and act accordingly
     */
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Destroyer")
        {
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "EnergyMovementCollider")
        {
            setCollidedWithEnergy(true);

        }
        if (other.gameObject.tag == "Destruction Zone" && !successPlaced)
        {
            Destroy(gameObject);
        }

        if (other.gameObject.tag == "EnergyPlacer")
        {
            Vector3 position = GameObject.Find("Place Energy").transform.position;
            position += Vector3.back * 2;
            transform.position = position;
            moveUp = true;
            successPlaced = true;
            if (hasDuplicate)
            {
                Destroy(duplicate);
                hasDuplicate = false;
            }
            
        }
        if (other.gameObject.tag == "DirectionChooser" && moveUp)
        {
            moveUp = false;
            bool dir = switchHolder.GetComponent<Switch>().GetDirection();
            if (dir)    //default direction is left
            {
                moveLeft = true;
            }
            if(!dir)
            {
                moveRight = true;
            }

        }
    }


    /* InitialMovement()
     * controls the initial movement behavior of energy, when it moves between selection slots
     * energy will move from the left to the right, as long as more energy comes to displace it
     */
    private void InitialMovement()
    {
        secondsWait = energySpawner.GetComponent<spawner>().getSpawnDelay();

        if (slotPosition == 0)
        {
            slotPos = slots[slotPosition].transform.position;
            haveMoved = true;
            transform.position = Vector3.MoveTowards(transform.position, slotPos, speed * Time.deltaTime);

        }
        if (slotPosition < 3 && collidedWithEnergy)
        {

            slotPos = slots[slotPosition].transform.position;
            haveMoved = true;
            transform.position = Vector3.MoveTowards(transform.position, slotPos, speed * Time.deltaTime);
        }

        if (slotPosition >= 3 && collidedWithEnergy)
        {
            trashPos = trash.transform.position;
            haveMoved = true;
            transform.position = Vector3.MoveTowards(transform.position, trashPos, speed * Time.deltaTime);

        }

        if (counter >= (secondsWait * 50))
        {
            counter = 0;

            if (haveMoved)
            {
                slotPosition++;
                haveMoved = false;
            }
 
            collidedWithEnergy = false;
            return;
        }
        counter++;
    }

    /* UpwardMovement()
     * specifies upward movement along conveyor belt
     */
    private void UpwardMovement()
    {
        transform.position += Vector3.up * Time.deltaTime * moveMultiplier;
    }

    /* LeftMove()
     * moves the energy unit left
     */
    private void LeftMove()
    {
        transform.position += Vector3.left * Time.deltaTime * moveMultiplier;
    }
    /* RightMove()
     * moves the energy unit right
     */
    private void RightMove()
    {
        transform.position += Vector3.right * Time.deltaTime * moveMultiplier;
    }


    /* OnMouseDown()
     * specifies behavior when clicking down on an energy object
     * creates a duplicate if necessary so that energy can disappear and "snap back" onto the slots if it is not placed correctly
     */
    private void OnMouseDown()
    {
        pointer = UnityEngine.Camera.main.WorldToScreenPoint(transform.position);

        if (!hasDuplicate)
        {
            GameObject duplicate = Instantiate(rootObj);
            this.duplicate = duplicate;
            duplicate.GetComponent<energy>().slotPosition = slotPosition + 1;
            col.enabled = false;
            transform.GetChild(0).gameObject.SetActive(false);
            hasDuplicate = true;
            checkForDup = true;
        }


    }
    /* setCollisionEnabled(bool choice)
     * setter for enabling/disabling collision
     */
    public void setCollisionEnabled(bool choice)
    {
        if (choice == true)
        {
            col.enabled = true;
        }
        if (choice == false)
        {
            col.enabled = false;
        }
    }

    /* OnMouseDrag()
     * moves the clicked on energy as the mouse is dragged
     */
    void OnMouseDrag()
    {
        initialMove = false;
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, pointer.z);
        Vector3 curPosition = UnityEngine.Camera.main.ScreenToWorldPoint(curScreenPoint);
        transform.position = curPosition;
    }

    /* OnMouseUp()
     * re-enables collision so that energy can detect if it is in a valid placement position.
     */
    private void OnMouseUp()
    {
        col.enabled = true;
    }
}
