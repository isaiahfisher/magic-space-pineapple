﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class energyBar : MonoBehaviour
{
    [SerializeField]
    private Image foreground;   //foreground image

    [SerializeField]
    private float updateSpeedSeconds = 0.5f;    //used to add smoothness to the energy bar movement


    /* Awake()
     * Get component in the parent object
     */
    private void Awake()
    {
        GetComponentInParent<generator>().OnEnergyChange += HandleEnergyChange;
    }

    /* HandleEnergyChange(float pct)
     * start the coroutine ChangeToPct
     */
    private void HandleEnergyChange(float pct)
    {
        StartCoroutine(ChangeToPct(pct));
    }

    /* ChangeToPct(float pct)
     * coroutine to get the energy in the generator as a percentage and adjust the energy bar accordingly
     */
    private IEnumerator ChangeToPct(float pct)
    {
        float preChangePct = foreground.fillAmount;
        float elapsed = 0f;

        while (elapsed < updateSpeedSeconds)
        {
            elapsed += Time.deltaTime;
            foreground.fillAmount = Mathf.Lerp(preChangePct, pct, elapsed / updateSpeedSeconds);
            yield return null;
        }

        foreground.fillAmount = pct;
    }
}
