﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class skillTreeScreen : MonoBehaviour
{
    public GameObject fuelButton;
    public GameObject switchButton;
    public GameObject minigameButton;
    public GameObject pilotDamageButton;
    public GameObject FuelEfficiencyButton;
    public GameObject InfAmmoButton;
    public GameObject LargeFuelButton;
    public Text currentFuel;
    public GameObject fuelTank;

    public bool fuelFlag = false;
    public bool switchFlag = false;
    public bool miniFlag = false;
    public bool pilotFlag = false;
    public bool effFlag = false;
    public bool ammoFlag = false;
    public bool largeFlag = false;

    // Start is called before the first frame update
    void Start()
    {
        fuelButton = GameObject.Find("Fuel Generation");
        switchButton = GameObject.Find("Switch CoolDown");
        minigameButton = GameObject.Find("MiniGame Gen");
        pilotDamageButton = GameObject.Find("PilotReduceDamage");
        FuelEfficiencyButton = GameObject.Find("Fuel Drain Reduced");
        InfAmmoButton = GameObject.Find("InfAmmo");
        LargeFuelButton = GameObject.Find("AllLargeFuel");
        fuelTank = GameObject.Find("Fuel Tank");

        fuelButton.GetComponent<Button>().interactable = false;
        switchButton.GetComponent<Button>().interactable = false;
        minigameButton.GetComponent<Button>().interactable = false;
        pilotDamageButton.GetComponent<Button>().interactable = false;
        FuelEfficiencyButton.GetComponent<Button>().interactable = false;
        InfAmmoButton.GetComponent<Button>().interactable = false;
        LargeFuelButton.GetComponent<Button>().interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        int fuel = fuelTank.GetComponent<generator>().getEnergy();
        currentFuel.text = fuel.ToString();

        //Debug.Log(fuelButton.GetComponent<perkButton>().getStates());

        if (fuel >= 100 && fuelButton.GetComponent<perkButton>().getStates() == 0)
        {
            fuelButton.GetComponent<Button>().interactable = true;
            fuelButton.GetComponent<perkButton>().setStates(1);
        }

        if (fuelButton.GetComponent<perkButton>().getStates() == 2 && !fuelFlag)
        {
            //Call method on spawner to passively generate fuel (will have to allow spawner to run out for this to work)
            fuelFlag = true;
        }

        if (fuel >= 200 && fuelFlag)
        {
            if (switchButton.GetComponent<perkButton>().getStates() == 0)
            {
                switchButton.GetComponent<Button>().interactable = true;
                switchButton.GetComponent<perkButton>().setStates(1);
            }
            if (minigameButton.GetComponent<perkButton>().getStates() == 0)
            {
                minigameButton.GetComponent<Button>().interactable = true;
                minigameButton.GetComponent<perkButton>().setStates(1);
            }
            if (pilotDamageButton.GetComponent<perkButton>().getStates() == 0)
            {
                pilotDamageButton.GetComponent<Button>().interactable = true;
                pilotDamageButton.GetComponent<perkButton>().setStates(1);
            }
        }

        if (switchButton.GetComponent<perkButton>().getStates() == 2 && !switchFlag)
        {
            //Call method on switch to remove delay (to do: add delay to switch so this does something
            switchFlag = true;
        }
        if (minigameButton.GetComponent<perkButton>().getStates() == 2 && !miniFlag)
        {
            //Call method on mp output to give more minigames #todo
            miniFlag = true;
        }
        if (pilotDamageButton.GetComponent<perkButton>().getStates() == 2 && !pilotFlag)
        {
            //Call method on mp output to reduce pilot damage #todo
            pilotFlag = true;
        }

        if ((fuel >= 250 && switchFlag) || (fuel >= 250 && miniFlag) || (fuel >= 250 && pilotFlag))
        {
            if (FuelEfficiencyButton.GetComponent<perkButton>().getStates() == 0)
            {
                FuelEfficiencyButton.GetComponent<Button>().interactable = true;
                FuelEfficiencyButton.GetComponent<perkButton>().setStates(1);
            }

        }
        if (FuelEfficiencyButton.GetComponent<perkButton>().getStates() == 2 && !effFlag)
        {
            //Call method to reduce fuel drain #todo
            effFlag = true;
        }

        if (fuel >= 400 && effFlag)
        {
            if (InfAmmoButton.GetComponent<perkButton>().getStates() == 0)
            {
                InfAmmoButton.GetComponent<Button>().interactable = true;
                InfAmmoButton.GetComponent<perkButton>().setStates(1);
            }

            if(LargeFuelButton.GetComponent<perkButton>().getStates() == 0)
            {
                LargeFuelButton.GetComponent<Button>().interactable = true;
                LargeFuelButton.GetComponent<perkButton>().setStates(1);
            }


        }
        if (InfAmmoButton.GetComponent<perkButton>().getStates() == 2 && !ammoFlag)
        {
            //call method to give pilot infinite ammo #todo
            ammoFlag = true;
        }
        if (LargeFuelButton.GetComponent<perkButton>().getStates() == 2 && !largeFlag)
        {
            //call method on spawner to set all fuel spawns to large #todo
            largeFlag = true;
        }
    }
}
