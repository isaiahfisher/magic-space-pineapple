﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BackButton : MonoBehaviour
{
    public Button backBtn;
    public GameObject tree;

    void Start()
    {
        Button btn = backBtn.GetComponent<Button>();
        GameObject tree = GameObject.Find("Upgrade Canvas");
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        tree.SetActive(false);
    }
}