﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{

    private bool left = true;   //default direction is left

    /* OnMouseDown()
     * rotate switch to correct direction
     * set left to correct value
     */
    private void OnMouseDown()
    {
        if (left)
        {
            transform.Rotate(0,0,-90);
            left = false;
            return;
        }
        if (!left)
        {
            transform.Rotate(0,0,90);
            left = true;
            return;
        }

    }

    /* GetDirection()
     * left getter
     */
    public bool GetDirection()
    {
        if (left)
        {
            return true;
        }
        else
            return false;
    }
}
