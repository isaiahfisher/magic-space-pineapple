﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class subsys : MonoBehaviour
{



    public GameObject myGenerator;      //generator associated with this subsystem

    public GameObject neighbor1;        //"neighbor" subsystem on the same generator

    public GameObject neighbor2;        //another "neighbor" subsystem on the same generator

    [SerializeField]
    private int secondsToGetEnergy = 5; //seconds until this subsystem requests energy

    private int energyDepleted = 5;     //amount of energy drained from generator

    private int tickCounter = 0;        //tickCounter for timing

    private bool value;                 //whether or not the subsystem got energy

    private generator gen;              //generator, for script access

    private subsys n1;                  //neighbor1, for script access

    private subsys n2;                  //neighbor2, for script access

    [SerializeField]
    private int energyDecreaseAmount = 5;   //energy to be reduced on the generator

    private bool shouldDrain = false;   //should this subsystem drain energy

    private bool isSelected = false;    //check if this subsystem is selected

    [SerializeField]
    private int randomRangeBot;         //bottom of random range

    [SerializeField]
    private int randomRangeTop;         //top of random range

    [SerializeField]
    private int probability;            //if this equals the randomly generated number, set shouldDrain = true

    System.Random rnd = new System.Random();    //random number

    Material m_Material;                //subsystem's material

    /* Start()
     * get scripts from generator and neighbors
     * get material
     * range checking for randomRangeTop, randomRangeBot, and probability
     */
    private void Start()
    {
        gen = myGenerator.GetComponent<generator>();

        n1 = neighbor1.GetComponent<subsys>();

        n2 = neighbor2.GetComponent<subsys>();

        m_Material = GetComponent<Renderer>().material;

        if (randomRangeBot > randomRangeTop)    //in case the random ranges are wrong
        {
            int tmp = randomRangeBot;
            randomRangeBot = randomRangeTop;
            randomRangeTop = tmp;
        }
        if (probability > randomRangeTop)
        {
            probability = randomRangeTop;
        }
        if (probability < randomRangeBot)
        {
            probability = randomRangeBot;
        }
    }

    /* setDecreaseAmount(int amount)
     * energyDecreaseAmount setter
     */
    public void setDecreaseAmount(int amount)
    {
        energyDecreaseAmount = amount;
    }

    /* getValue()
     * value getter
     */
    public bool getValue()
    {
        return value;
    }

    public void setValueF()
    {
        value = false;
    }

    /* OnMouseDown()
     * if not selected, increase size to visually indicate that this subsystem is selected
     * set isSelected to true
     * set neighbors' isSelected to false
     */
    private void OnMouseDown()
    {
        if (!isSelected)
        {
            transform.localScale += new Vector3(0.1F, 0.1F, 0.1F);
        }
        isSelected = true;
        n1.setIsSelectedFalse();
        n2.setIsSelectedFalse();
    }

    /* setIsSelectedFalse()
     * if selected, set isSelected to false
     * decrease size to original size
     */
    private void setIsSelectedFalse()
    {
        if (isSelected)
        {
            isSelected = false;
            transform.localScale -= new Vector3(0.1F, 0.1F, 0.1F);
        }
    }

    /* FixedUpdate()
     * Change color if subsystem needs energy
     * Indicate if subsystem recieves energy or is deprived of energy
     */
    private void FixedUpdate()
    {

        int ticksToGetEnergy = secondsToGetEnergy * 50;

        int ticksToAlert = ticksToGetEnergy - 100;  //warning 2 seconds before energy is taken, can change if necessary

        if (tickCounter == ticksToAlert)
        {
            int randomVal = rnd.Next(randomRangeBot, randomRangeTop + 1);
            if (probability == randomVal)
            {
                m_Material.color = Color.yellow;
                shouldDrain = true;
            }
            else
            {
                m_Material.color = Color.white;
            }
        }

        if (tickCounter == ticksToGetEnergy && shouldDrain)
        {
            int energyLeft = gen.getEnergy();
            if (energyLeft >= energyDepleted && isSelected)
            {
                gen.decreaseEnergy(energyDecreaseAmount);
                value = true;
                m_Material.color = Color.green;
                isSelected = false;
                transform.localScale -= new Vector3(0.1F, 0.1F, 0.1F);
            }
            else
            {
                value = false;
                m_Material.color = Color.red;
            }
            shouldDrain = false;
            tickCounter = 0;
        }
        if (tickCounter == ticksToGetEnergy && !shouldDrain)
        {
            m_Material.color = Color.white;
            tickCounter = 0;
        }
        tickCounter++;
    }
}
