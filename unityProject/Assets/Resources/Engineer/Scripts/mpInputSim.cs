﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mpInputSim : MonoBehaviour
{
    public GameObject energySpawner;

    [SerializeField]
    private int energyAddAmount = 5;

    void Start()
    {
        energySpawner = GameObject.Find("Energy Spawner");
    }

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            energySpawner.GetComponent<spawner>().setPickedUpEnergy(energyAddAmount);
        }
    }
}
