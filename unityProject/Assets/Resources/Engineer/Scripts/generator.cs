﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class generator : MonoBehaviour
{
    public int startingEnergy = 50; //starting energy

    public event System.Action<float> OnEnergyChange = delegate { };    //delegate for energy bar functionality

    public Text energyLeft;         //energy left output in text

    public Text energyMax;          //maximum energy output in text

    [SerializeField]
    private int maxEnergy = 100;    //maximum energy

    [SerializeField]
    private int minEnergy = 0;      //minimum energy

    private int currentEnergy;      //current energy

    private energy eng;             //eng of energy unit entering generator


    /* Start()
     * Assign starting energy
     * Set text for currentEnergy and maxEnergy
     * Set currentEnergyPct
     * Call OnEnergyChange with argument currentEnergyPct
     */
    void Start()
    {
        currentEnergy = startingEnergy;

        energyLeft.text = currentEnergy.ToString();

        energyMax.text = maxEnergy.ToString();

        float currentEnergyPct = (float)currentEnergy / (float)maxEnergy;
        OnEnergyChange(currentEnergyPct);
    }

    /* OnTriggerEnter(Collider other)
     * When energy object contacts the generator, add that object's energy value to the currentEnergy not to exceed maxEnergy
     * Update energyLeft UI element
     * Calculate currentEnergyPct
     * Call OnEnergyChange with argument currentEnergyPct
     * Destroy the energy object
     */
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Energy")
        {
            eng = other.GetComponent<energy>();
            if (currentEnergy + eng.energyAmount < maxEnergy)
            {
                currentEnergy += eng.energyAmount;
            }
            else
            {
                currentEnergy = maxEnergy;
            }

            energyLeft.text = currentEnergy.ToString();

            float currentEnergyPct = (float)currentEnergy / (float)maxEnergy;
            OnEnergyChange(currentEnergyPct);

            Destroy(other.gameObject);
        }
    }

    /* getEnergy()
     * currentEnergy getter
     */
    public int getEnergy()
    {
        return currentEnergy;
    }

    /* decreaseEnergy(int amount)
     * decrease currentEnergy by the specified amount not less than minEnergy
     * Update energyLeft UI element
     * Calculate currentEnergyPct
     * Call OnEnergyChange with argument currentEnergyPct
     */
    public void decreaseEnergy(int amount)
    {
        currentEnergy = Mathf.RoundToInt(currentEnergy - amount);
        if (currentEnergy < minEnergy)
        {
            currentEnergy = minEnergy;
        }

        energyLeft.text = currentEnergy.ToString();

        float currentEnergyPct = (float)currentEnergy / (float)maxEnergy;
        OnEnergyChange(currentEnergyPct);
    }
}
