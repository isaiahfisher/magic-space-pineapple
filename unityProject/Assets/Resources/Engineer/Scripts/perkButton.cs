﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class perkButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public enum States {unavailable, available, activated};

    private States state = 0;

    public GameObject fuelTank;
    public Button pkButton;

    public string buttonInfo = "sample text";

    public int fuelCost = 100;

    public Text infoBox;



    // Start is called before the first frame update
    void Start()
    {
        fuelTank = GameObject.Find("Fuel Tank");
        Button btn = pkButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
        //Debug.Log(this.name);
    }

    // Update is called once per frame
    void Update()
    {
        if (fuelTank.GetComponent<generator>().getEnergy() < fuelCost && (state == (States)1))
        {
            setStates(0);
            pkButton.interactable = false;
        }
    }

    public void setStates(int status)
    {
        state = (States) status;
    }

    public int getStates()
    {
        if (state == (States) 0)
        {
            return 0;
        }
        if (state == (States) 1)
        {
            return 1;
        }
        if (state == (States) 2)
        {
            return 2;
        }
        else return -1;
    }

    void TaskOnClick()
    {
        if (state != (States) 2)
        {
            fuelTank.GetComponent<generator>().decreaseEnergy(fuelCost);
            setStates(2);
            GetComponent<Image>().color = Color.green;
        }

        Debug.Log(this.name);
        Debug.Log(getStates());
        //pkButton.interactable = false;



    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        infoBox.text = buttonInfo;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        infoBox.text = "";
    }
}
