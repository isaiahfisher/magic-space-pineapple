﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mpOutputSim : MonoBehaviour
{
    public GameObject shield;
    public GameObject gravity;
    public GameObject puzzDiff;
    public GameObject ammo;
    public GameObject thrust;
    public GameObject minigameGen;

    public bool shieldVal;
    //private bool shieldDrain;
    private bool gravVal;
    private bool puzzVal;
    private bool ammoVal;
    private bool thrustVal;
    private bool miniVal;

    

    // Start is called before the first frame update
    void Start()
    {
        shield = GameObject.Find("Shield");
        gravity = GameObject.Find("Gravity");
        puzzDiff = GameObject.Find("Puzzle Difficulty");
        ammo = GameObject.Find("Ammo");
        thrust = GameObject.Find("Thrust");
        minigameGen = GameObject.Find("Minigame Gen");

        shieldVal = shield.GetComponent<subsys>().getValue();
        gravVal = gravity.GetComponent<subsys>().getValue();
        puzzVal = puzzDiff.GetComponent<subsys>().getValue();
        ammoVal = ammo.GetComponent<subsys>().getValue();
        thrustVal = thrust.GetComponent<subsys>().getValue();
        miniVal = minigameGen.GetComponent<subsys>().getValue();

}

    void FixedUpdate()
    {
        shieldVal = shield.GetComponent<subsys>().getValue();
        gravVal = gravity.GetComponent<subsys>().getValue();
        puzzVal = puzzDiff.GetComponent<subsys>().getValue();
        ammoVal = ammo.GetComponent<subsys>().getValue();
        thrustVal = thrust.GetComponent<subsys>().getValue();
        miniVal = minigameGen.GetComponent<subsys>().getValue();

        if (shieldVal)
        {
            Debug.Log("shield got energy");
            shield.GetComponent<subsys>().setValueF();
        }

        if (gravVal)
        {
            Debug.Log("gravity got energy");
            gravity.GetComponent<subsys>().setValueF();
        }

        if (puzzVal)
        {
            Debug.Log("puzzle got energy");
            puzzDiff.GetComponent<subsys>().setValueF();
        }

        if (ammoVal)
        {
            Debug.Log("ammo got energy");
            ammo.GetComponent<subsys>().setValueF();
        }

        if (thrustVal)
        {
            Debug.Log("thrust got energy");
            thrust.GetComponent<subsys>().setValueF();
        }

        if (miniVal)
        {
            Debug.Log("minigame got energy");
            minigameGen.GetComponent<subsys>().setValueF();
        }

    }
}
