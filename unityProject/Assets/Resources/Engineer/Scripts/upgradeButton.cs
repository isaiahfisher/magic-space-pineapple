﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class upgradeButton : MonoBehaviour
{
    public Button upgradeBtn;
    public GameObject tree;

    void Start()
    {
        Button btn = upgradeBtn.GetComponent<Button>();
        GameObject tree = GameObject.Find("Upgrade Canvas");
        btn.onClick.AddListener(TaskOnClick);
        tree.SetActive(false);
    }

    void TaskOnClick()
    {
        tree.SetActive(true);
    }
}