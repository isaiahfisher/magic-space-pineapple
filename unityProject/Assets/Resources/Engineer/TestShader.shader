// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "TestShader"
{
	Properties
	{
		_ColorMix("Color Mix", Range( 0 , 1)) = 0
		_SpecularLevel("Specular Level", Range( 0 , 1)) = 0
		_SmoothnessLevel("Smoothness Level", Range( 0 , 1)) = 0
		_rock_n("rock_n", 2D) = "bump" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf StandardSpecular keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _rock_n;
		uniform float4 _rock_n_ST;
		uniform float _ColorMix;
		uniform float _SpecularLevel;
		uniform float _SmoothnessLevel;

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float2 uv_rock_n = i.uv_texcoord * _rock_n_ST.xy + _rock_n_ST.zw;
			o.Normal = UnpackNormal( tex2D( _rock_n, uv_rock_n ) );
			float4 color4 = IsGammaSpace() ? float4(0.5283019,0.5283019,0.5283019,0) : float4(0.2411783,0.2411783,0.2411783,0);
			float4 color3 = IsGammaSpace() ? float4(0,0.05708027,1,0) : float4(0,0.004603244,1,0);
			float4 lerpResult2 = lerp( color4 , color3 , _ColorMix);
			o.Albedo = lerpResult2.rgb;
			float3 temp_cast_1 = (_SpecularLevel).xxx;
			o.Specular = temp_cast_1;
			o.Smoothness = _SmoothnessLevel;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17400
334.4;154.4;1045;476;521.1159;86.8407;1;False;True
Node;AmplifyShaderEditor.ColorNode;4;-597.8115,-19.98624;Inherit;False;Constant;_Color1;Color 1;0;0;Create;True;0;0;False;0;0.5283019,0.5283019,0.5283019,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;3;-598.9978,205.4632;Inherit;False;Constant;_Color0;Color 0;0;0;Create;True;0;0;False;0;0,0.05708027,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;5;-642.9008,387.0096;Inherit;False;Property;_ColorMix;Color Mix;0;0;Create;True;0;0;False;0;0;0.4033519;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;2;-285.905,81.06229;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-316.8628,250.0299;Inherit;False;Property;_SpecularLevel;Specular Level;1;0;Create;True;0;0;False;0;0;0.5087869;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8;-317.3341,347.8857;Inherit;False;Property;_SmoothnessLevel;Smoothness Level;2;0;Create;True;0;0;False;0;0;0.6437434;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;31;-495.1425,-222.7761;Inherit;True;Property;_rock_n;rock_n;3;0;Create;True;0;0;False;0;-1;0bebe40e9ebbecc48b8e9cfea982da7e;0bebe40e9ebbecc48b8e9cfea982da7e;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;-1;2;ASEMaterialInspector;0;0;StandardSpecular;TestShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;2;0;4;0
WireConnection;2;1;3;0
WireConnection;2;2;5;0
WireConnection;0;0;2;0
WireConnection;0;1;31;0
WireConnection;0;3;6;0
WireConnection;0;4;8;0
ASEEND*/
//CHKSM=440A0DE441C2A66DA2BC3B9E7A825943E07C6ED7